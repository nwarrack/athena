/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// CurvilinearParametersT.icc, (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////

//Gaudi
#include "GaudiKernel/MsgStream.h"
//Trk
#include "TrkEventPrimitives/ParamDefs.h"

namespace Trk
{
// Constructor with TP arguments
  template <int DIM,class T,class S>
  Trk::CurvilinearParametersT<DIM,T,S>::CurvilinearParametersT(const AmgVector(DIM+2)& parameters,
							       AmgSymMatrix(DIM)* covariance,
							       unsigned int cIdentifier) :
    ParametersT<DIM,T,S>(Amg::Vector3D(parameters[x],parameters[y],parameters[z]),
			 Amg::Vector3D(parameters[3],parameters[4],parameters[5]),
			 covariance),
    m_cIdentifier(cIdentifier)
  {
    // flip the charge according to qOverP
    if (parameters[6] < 0.) this->m_chargeDef.setCharge(-1);
    // assign the parameters
    this->m_parameters[locX]   = 0.;
    this->m_parameters[locY]   = 0.;
    // get phi & theta from the momentum vector
    this->m_parameters[phi]    = this->momentum().phi();
    this->m_parameters[theta]  = this->momentum().theta();
    this->m_parameters[qOverP] = parameters[6]/this->momentum().mag();
  }
  
  // Constructor with TP arguments
  template<int DIM,class T,class S>
  CurvilinearParametersT<DIM,T,S>::CurvilinearParametersT(const Amg::Vector3D& pos,
							double tphi,
							double ttheta,
							double tqOverP,
							AmgSymMatrix(DIM)* cov,
							unsigned int cIdentifier):
    ParametersT<DIM,T,S>(),
    m_cIdentifier(cIdentifier)
  {
    this->m_position   = pos;
    this->m_covariance = cov;

    // flip the charge according to qOverP
    if(tqOverP < 0.)
      this->m_chargeDef.setCharge(-1.);
    else
      this->m_chargeDef.setCharge(1.);
    
    // assign the parameters
    this->m_parameters[Trk::locX]   = 0.;
    this->m_parameters[Trk::locY]   = 0.;
    this->m_parameters[Trk::phi]    = tphi;
    this->m_parameters[Trk::theta]  = ttheta;
    this->m_parameters[Trk::qOverP] = tqOverP;

    // make sure that the position & momentum are calculated
    double p = fabs(1./tqOverP);
    this->m_momentum = Amg::Vector3D(p*cos(tphi)*sin(ttheta),p*sin(tphi)*sin(ttheta),p*cos(ttheta));

  }

  // full global constructor
  template<int DIM,class T,class S>
  CurvilinearParametersT<DIM,T,S>::CurvilinearParametersT(const Amg::Vector3D& pos,
							const Amg::Vector3D& mom,
							double charge,
							AmgSymMatrix(DIM)* cov,
							unsigned int cIdentifier) :
    ParametersT<DIM,T,S>(),
    m_cIdentifier(cIdentifier)    
  {
    this->m_chargeDef.setCharge(charge);
    this->m_covariance = cov;

    // assign the parameters
    this->m_parameters[Trk::locX]   = 0.;
    this->m_parameters[Trk::locY]   = 0.;
    this->m_parameters[Trk::phi]    = mom.phi();
    this->m_parameters[Trk::theta]  = mom.theta();

    if(charge == 0.)
      charge = 1.; // such that below is 1./mom.mag()
    
    this->m_parameters[Trk::qOverP] = charge/mom.mag();
    this->m_position = pos;
    this->m_momentum = mom;
  }

  // copy constructor
  template<int DIM,class T,class S>
  CurvilinearParametersT<DIM,T,S>::CurvilinearParametersT(const CurvilinearParametersT<DIM,T,S>& copy):
    ParametersT<DIM,T,S>(copy),
    m_cIdentifier(copy.m_cIdentifier)
  {}
							
  // move constructor
  template<int DIM,class T,class S>
  CurvilinearParametersT<DIM,T,S>::CurvilinearParametersT(CurvilinearParametersT<DIM,T,S>&& copy):
    ParametersT<DIM,T,S>(std::move(copy)),
    m_cIdentifier(copy.m_cIdentifier)
  {
  }
  
  // Assignment operator
  template<int DIM,class T,class S>
  CurvilinearParametersT<DIM,T,S>& CurvilinearParametersT<DIM,T,S>::operator=(const CurvilinearParametersT<DIM,T,S>& rhs)
  {
    if(this != &rhs)
    {
      ParametersT<DIM,T,S>::operator=(rhs);
      // and the curvilinear identifier 
      m_cIdentifier = rhs.m_cIdentifier;
    }
  
    return *this;
  }

  // Move assignment operator
  template<int DIM,class T,class S>
  CurvilinearParametersT<DIM,T,S>& CurvilinearParametersT<DIM,T,S>::operator=(CurvilinearParametersT<DIM,T,S>&& rhs)
  {
    if(this != &rhs)
    {
      ParametersT<DIM,T,S>::operator=(std::move(rhs));
      // and the curvilinear identifier 
      m_cIdentifier = std::move(rhs.m_cIdentifier);
    }
  
    return *this;
  }

  // equality operator
  template<int DIM,class T,class S>
  bool CurvilinearParametersT<DIM,T,S>::operator==(const ParametersBase<DIM,T>& rhs) const
  {
    // tolerance for comparing matrices/vector
    static const double& tolerance = 1e-8;
    
    // make sure we compare objects of same type
    decltype(this) pCasted = dynamic_cast<decltype(this)>(&rhs);
    if(!pCasted)
      return false;

    // comparison to myself?
    if(pCasted == this)
      return true;

    // compare identifier
    if(cIdentifier() != pCasted->cIdentifier())
      return false;

    // compare UVT frame
    CurvilinearUVT local_curvilinearFrame=curvilinearFrame();
    CurvilinearUVT casted_curvilinearFrame=pCasted->curvilinearFrame();
    if(!local_curvilinearFrame.curvU().isApprox(casted_curvilinearFrame.curvU(),tolerance))
      return false;
    if(!local_curvilinearFrame.curvV().isApprox(casted_curvilinearFrame.curvV(),tolerance))
      return false;
    if(!local_curvilinearFrame.curvT().isApprox(casted_curvilinearFrame.curvT(),tolerance))
      return false;
    
    // compare equality of base class parts
    return ParametersT<DIM,T,S>::operator==(rhs);
  }
  
  template<int DIM,class T,class S>
  const S& CurvilinearParametersT<DIM,T,S>::associatedSurface() const
  {
    if(!this->m_surface)
    {
      // create the surface for global position and global rotation
      const_cast<S*&>(this->m_surface) = new S(this->position(),curvilinearFrame());
    }

    return (*this->m_surface);
  }
  
  // Screen output dump
  template<int DIM,class T,class S>
  MsgStream& CurvilinearParametersT<DIM,T,S>::dump(MsgStream& out) const
  {
    out << "CurvilinearParametersT parameters:"  << std::endl;
    ParametersT<DIM,T,S>::dump(out);
  
    return out;
  }

  // Screen output dump
  template<int DIM,class T,class S>
  std::ostream& CurvilinearParametersT<DIM,T,S>::dump(std::ostream& out) const
  {
    out << "CurvilinearParametersT parameters:" << std::endl;
    ParametersT<DIM,T,S>::dump(out);
  
    return out;
  }


  // Surface return (with on demand construction)
  template <int DIM,class T,class S>
  const Amg::RotationMatrix3D CurvilinearParametersT<DIM,T,S>::measurementFrame() const
  {
    static Amg::RotationMatrix3D mFrame;
    // the columnes
    CurvilinearUVT local_curvilinearFrame=curvilinearFrame();
    mFrame.col(0) = local_curvilinearFrame.curvU();
    mFrame.col(1) = local_curvilinearFrame.curvV();
    mFrame.col(2) = local_curvilinearFrame.curvT();

    // return the rotation matrix that defines the curvilinear parameters
    return mFrame;
  }

  template<int DIM,class T,class S>
  const CurvilinearUVT CurvilinearParametersT<DIM,T,S>::curvilinearFrame() const
  {
    CurvilinearUVT curvilinFrame(this->momentum().unit());
    return curvilinFrame;
  }

  // Dedicated update method for non-curvilinear parameters - private and controlled by friendship 
  template <int DIM,class T,class S>
  void Trk::CurvilinearParametersT<DIM,T,S>::updateParameters(const AmgVector(DIM)& updatedParameters,
							      AmgSymMatrix(DIM)* updatedCovariance)
  {
    // valid to use != here, because value is either copied or modified,
    bool updateMomentum = (updatedParameters[Trk::phi] != this->m_parameters[Trk::phi]) ||
      (updatedParameters[Trk::theta] != this->m_parameters[Trk::theta]) ||
      (updatedParameters[Trk::qOverP] != this->m_parameters[Trk::qOverP]);

    // update the covariance
    if (updatedCovariance)
    {
      if (updatedCovariance != this->m_covariance)
	delete this->m_covariance;
      this->m_covariance = updatedCovariance;
    }
    
    // momentum update is needed    
    if (updateMomentum)
    {
      double phi   = updatedParameters[Trk::phi];
      double theta = updatedParameters[Trk::theta];
      double p     = this->charge()/updatedParameters[Trk::qOverP];
      // assign them and update the momentum 3 vector
      this->m_parameters[Trk::phi]    = phi;
      this->m_parameters[Trk::theta]  = theta;
      this->m_parameters[Trk::qOverP] = updatedParameters[Trk::qOverP];
      this->m_momentum = Amg::Vector3D(p*cos(phi)*sin(theta),
						       p*sin(phi)*sin(theta),
						       p*cos(theta));
    }
    // position update if needed -  loc1
    if (updatedParameters[Trk::loc1] != 0.) 
      this->m_position += updatedParameters[Trk::loc1] * curvilinearFrame().curvU();
    // position update if needed -  loc2
    if (updatedParameters[Trk::loc2] != 0.) 
      this->m_position += updatedParameters[Trk::loc2] * curvilinearFrame().curvV();
    // in any case, the (eventually existing) surface is invalidated
    delete this->m_surface;
    this->m_surface = 0;
  }
} // end of namespace Trk
